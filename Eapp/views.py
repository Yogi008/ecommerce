from store.models import Product
from django.shortcuts import render
from .models import Category
from store.models import Product

# Create your views here.
def home(request):
    products  = Product.objects.all().filter(is_avaliable=True)
    context = {"products":products,}
    return render(request, "index.html",context)

